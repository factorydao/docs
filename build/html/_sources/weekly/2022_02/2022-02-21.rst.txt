Weekly Updates: February 21st, 2022
***********************************

**DAO Suite**

- Discussion on new dApps

  - Decision dApp. Fair election. Hiding votes while they are coming in. Votes are compiled and outcome is passed back to L1 chain and then contract is executed. 
  - Emergent suggestion system.

**Communications & Marketing**

- Preparing 6 Week Showcase

  - Grand finale
   
    - Possibilities? 
    - TribesDAO, polling in various realms.

  - Showcase planning meeting today at 2pm. 

- Distributing DAOs over time. 
- Demos for VCs. 
- How to setup a DAO with us. Step-by-step.
- Tweet writing tomorrow starting at 1pm UTC.
- Sales funnel meeting. Nick, Matt, Naved. Tuesday, 10:30 UTC.

  - 3 persons: Lead Generator, Closer, and Account Manager. 

    - Over time scale this to the hundreds.  

- Potential of targeting gaming guilds to onboard with us and invest in us. Where the users are at. Perfect for decentralised governance. 

**Community**

- This week's The Programme. 

  - Harberger taxes and yield.

- Getting community active in the next 3 weeks. How?
- Set of tasks for people to do like a checklist.

**Governance**

- Scheduling, planning, tasks, organizational strategies. Tuesday morning. Naved, Nick. 

  