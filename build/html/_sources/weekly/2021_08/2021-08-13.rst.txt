Weekly Updates: August 13th, 2021
*********************************

**Bank.vote & Airdrop**

- New engineer Patrick working on it with Marek. He is onboarding with our new documentation. 
- Vote mining design nearing completion.  
- Halborn auditing contract. 
- Some changes made to the contract. Some new automated tests. 
- Prepare for Ropsten network test to be done among team members.  

**Influence.vote**

- Ongoing work with partner. 
- What token next? vote is live. 

  - We all vote on it. Promote it. Be active. 
  - In Comms meeting, discuss how we can push more and create more excitement around it. 

- Team Virtue -- 3D Team

  - Refining to a scalable system. 
  - Flow diagram created on Miro. 
  - Bringing on new influencer as an advisor. 
  - Preparing our product more to assist other communities to run votes. 
  - Moving communications forward on this. Zebu. Marketing plan. Marketing deck.  
  - Sequential minting. 

- Discussion on new ways to internally test. 
- New potential partner wanting to onbaord to use Influence. Potentially to use full dApp suite for their DAO.

  - How they will use Influence. 
  - Building strategy. 
  - Pitch Deck.
  - Potential influence video. Chris C. 

- Creating dashboard for Admin? 

**Yield.vote**

- New tests have been run. 
- Design fixes. 
- Itrust wants to do another Basic Pool and Resident Pool. 

  - Prepare announcements for this.
  - Want to add 2 more pools. 

- ACTION: Make document explaining technical differences for new yield version. Technical Summary. Chris. 
- Prepare announcements for new yield launch. 
- ACTION: Complete Feature Benefit Matrix for yield. Chris. Matt. 
- Discussion on partnership fee.
- Resident Pool.​​​​​​​

  - ACTION: Naomi to write PR for Zebu. 

- Deployed contracts. Pools added. 
- Few frontend bugs being fixed. 
- Yield 2.0 launched on Aug 13th.

**Multi-chain**

- Bridge

  - When the server goes down, money is not lost. If tokens made it to the contract, they are safe. 

**Operations**

- Discussion on system for tech support. 
- Meeting flow and organization. 

**Community, Content, Marketing**

- Image for What token next? 
- Image for Bridge. 
- Announcements.
- Discussion on reorganizing our comms team and strategy. 
- Naomi to take on a more CMO role. 

**BizDev**

- Discussion on bounty for community members who help land us a working partnership. 

  - Land a bounty amount. 
