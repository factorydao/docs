Weekly Updates: May 9th, 2022
*****************************

**DAO Suite**

- **mint.vote**

  - Friday 13th FVT NFT ETH IDs available to mint. 
  - Continuation of original series. Capped at 1000 unless DAO ever votes to increase it.
  - With new mint price and decay rate. 
  - New art series and new art dropped to all previous FVT IDs that had only placeholder art. 
  - Working on the new frontend.
  - Plan to do for Polygon once we launch Markets on Polygon. 

    - ETH ID holders will have access to mint on Polygon. 

  - Exclusive benefits. Special Access. Alpha.

    - Roadmap of benefits. Chris C.

  - Work on announcements and planning asynchronously tomorrow. 
  - Discuss later a plan about what to do with BSC IDs. 
  - New minting page for FVT ID minting. 

- **influence.vote**

  - Setup DataDAO Realm. 
  - Ability to set up Submissions and Realms for Lizzl & Ksenya. 

- Prepare to launch our dApps on a new chain for upcoming partnerships. 

  - Incentivized test. Access to FVT ID holders.
  - Aiming for the 19th.
  - Full test.
  - Tuesday meeting to discuss. 

**Alliances**

- Prepare potential partnership SOWs.

**Communications & Marketing**

- Begin preparing partnership announcements. 

**Community**

- Open submissions for Community Updates. 
- FVT Discord fun, exclusive meetings.
- Come up with schedule of cool things happening in FVT Discord. 
- Gating FVT Discord Friday. Leave one channel open to everyone. 
- Meet this week to discuss MetaSherpa roles. 
- Meet this week to discuss how to communicate message about what is happening in gated FVT Discord. 
- Dework matters. Ksenya.

**SubDAOs**

- **DataDAO**

  - Present Gov drop analysis in FVT Discord. Alpha. 
