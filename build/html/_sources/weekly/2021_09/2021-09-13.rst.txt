Weekly Updates: September 13th, 2021
************************************

**Influence.vote**

- New Partner: Stoner Cats. 

  - Have agreed to use influence. 
  - Discussion on their DAO structure. 
  - Strategies and next steps. 
  - Zebu and marketing. 

- Potential Partner

  - Brand and product strategy, branding. Naomi. 
  - NFT art rendering. 
  - Rarity scheme. Look over the math and code. 
  - Minting strategy. 
  - Front-end. Interface. 
  - Strategy meeting Tuesday Sept. 14th.

**Yield.vote**

- Competitor analysis. Marta. 

**Governance**

- Final draft of governance framework. Then present to the community. Nick. 
- Weekly theme structure. Weekly Priorities. 
- New system. Master file of links to all working documents. 

**Community, Content, Marketing**

- Weekly Community Call.
- Discourse Radio. Joseph, Matt. 
- Compiling event list. Ksenya. 
- New community activities. Ksenya. Nick.
- Website. Marketing Expansion. 
- NFT deck for approaching NFT DAOs. 
- Friday call? Update or learning call? 
- A Weekly Programme. Start for next week. Announce at the beginning of the week, visual timetable.
- Newsletter strategy. 

**BizDev**

- Jobs Board. Matt.
- DAO Recruitment. Route to contribution. Matt, Nick. 

