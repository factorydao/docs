Weekly Updates: May 10th, 2021
******************************

**Yield.vote**
*Resident Pool (for Ethereum citizens)*

- Launch Sequence Finalized
- Incentivized test closes Friday 14th 12:00pm UTC
- Citizens Vote Closes Sunday 16th Midnight UTC
- Mainnet Launch: Monday 17th 12:00pm UTC
- Incentive for Resident Pool Test: get FVT rewards for the most amount of properties held for the longest. 
- Citizens vote to be held on influence.vote where citizens vote on parameters to be used for the mainnet launch. Dr. Nick will begin to curate debate in telegram citizens group
- Dr. Nick to write article on Resident Pool and citizens vote for Resident Pool
- Website visuals created
- Discussion on social media posts
- Bug Bounty: bug bounty being paid out to user who identified critical issue

**Markets.vote** 

- Discussing strategy on how citizens can propose new tokens to be added to markets.vote
- Prepare for “What Token Next?!” Influence vote using token weighted QV
- Fix BSC strategy 
- Working with Pokt
- Working with Ankr
- PropOther expensive provider came back and admitted their nodes can’t sync either 
- Infra providers claim to be close. Chain continues to show signs of being fundamentally rekt. 

**Auction.vote**

- Finalizing details for NFT auction to take place this month
- Thursday Dev meeting to discuss details of auction.vote

**LDN21 (A London Election Voting Experiment)**

- Meeting with a new press officer to create tabloid-like article about the LDN21 vote. 
- Continuing discussion with political groups
- Finalizing the parameters to be used in PDF that will showcase the analytics of the LDN21 vote

**Partnerships**

- Ongoing discussion with 5 potential partners
- Building governance proposal for key partner for this month.
- 3 partnership announcements in progress
- Build dApp suite deck for adopting new DAOs

**Marketing/Branding**

- Scheduled Dev meeting to establish branding to prepare for collaborations with PR partners for next Wednesday
- Communications strategy: deciding on our strategy to engage/recruit thought leaders. 

**Community Fund**

- Has been setup 
- Queue of transactions setup to pay out content creators
- Set up rolling quadratic voting system for payouts and bounty system

**Governance**

- Continuing work on Timeline to soon be made public 
- Continuing work on Governance Map to soon be made public
- Continuing work with lawyers to establish our legal entities 

**influence.vote**

- Bug Fix: “Winner” not showing on influence.vote. 
- Set up episode 2 ethereum citizens vote for use in designing yield parameters. 
- Release LDN21 results and push to press. 

**BSC Citizens**

- Create a community video vote in BSC chamber to help create an upcoming “degen friendly” launch video for BSC markets when functional. 








