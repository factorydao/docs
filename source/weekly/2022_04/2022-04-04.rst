Weekly Updates: April 4th, 2022
*******************************

**DAO Suite**

- **markets.vote**

  - Schedule chat to discuss possibilities of dropping art on IDs without art. 

- **yield.vote**

  - Basic Pool

    - Proposal Vote, “FVT Token Economics: Initiate New Token Emission Programme and Fact Drop”, Completed. 
    - Open Basic Pool and stake weighted voting.
    - How to attract new token holders?
    - Parameters.
    - Basic Pool with control over Resident Pool.
    - AirDrop in the future for participating. 
    - Stake weighted voting and product chat with Dev team. Monday.

- **bank.vote**

  - Governance Drop

    - Tech Review with devs. Aim for this week. 

**Alliances**

- Potential DAO interested in continuous Auction and Influence.

  - Demo on Tuesday. Nick, Naomi, Naved.  

- Potential partner to use our NFT DAO tooling. 

  - Will help them explore MetaDAO structures, creating interest specific subDAOS across the ecosystem, with their own governance and funding. 
  - Announcement. Nick.
  - Call with their community.
  - Supported one of their communities last week to launch their own NFT DAO.

- Tuesday meeting with Potential partner. Matt.

**Communications & Marketing**

- Discussion on current public engagement strategies. Where are we now? What changes do we want to make? 
- Showcase

  - A narrative we build over a few weeks?
  - Picking 1 audience (focused campaign) that we go heavy on per season such as NFT projects. 
  - Discussion on potential audiences. 
  - Sub audiences i.e. NFT projects, pre-mint NFT projects, etc. 
  - Build content upon research. 
  - Involve community in marketing strategy. 

- Rarity Tools? Advertising on Rarity Sniper Discord? 
- Content

  - FVT token economics. Nick. Monday. 
  - What is FactroyDAO? Zebu, Nick. Monday. 
  - Partnership announcement. Nick. 
  - Proposal for gating FVT Discord.

    - Should we gate FVT Discord with IDs? Change of Mint parameters.

  - Liquidity mining piece.
  - Convert written pieces to videos. Chris C., Nick. Tuesday. 
  - Other short pieces? 
  - Substack? Rename or create new one? 
  - Where to post videos?
  - Announcement package. 

- Marketing Strategy Call Tuesday.  Matt, Ksenya, Nick. 

  - Decide on bounties 

**Community**

- SubDAOs now funded.
