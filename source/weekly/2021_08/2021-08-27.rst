Weekly Updates: August 27th, 2021
*********************************

**Bank.vote & Airdrop**

- Stable link. On Ropsten. On schedule. 
- Need to finalize the addresses and token amounts. 
- Airdrop Announcement

  - Technical details. How we decided on these details. 

    - Write up.
 
  - Vote mining. Incentivize people to participate in governance. 

- Discussion on decentralised payroll. 
- Discussion on Ropsten Test. 
- Schedule meeting to further discuss bank next week. Joint QA with the team. 
- Halborn audit on airdrop contract. 

**Influence.vote**

- Potential partner.

  - Joining one of their community calls. 

- Discussion. Should aggregate vote totals be shown while a vote is running? 
- Potential Partner

  - Preparing marketing deck.

- Potential Partner

  - Discussion.

- Making URL where you can download csv for What token next vote.
- Discussion on funs votes. 
- People are wanting votes in their DAOs on what to do with treasury funds such as purchasing NFTs or not.
- What is Influence 2.0? Make the coolest Influence app. Campaign. Outreach. 

**Yield.vote**

- Ropsten Test

  - Contracts are deployed and pools are added. 
  - Where to get rFVT and rSLP? Links. 
  - Monday Aug 23rd Start.

- Itrust Pools​​​​​​​

  - Halborn audit. 

- Zebu PR. Announcement document. Ksenya. Naomi. 
- Prepare Discord, Telegram, Twitter announcements. 
- Purchase new Ethtrader banner. 

**Operations**

- Payroll discussion.

**Governance**

- Invite engineering team to Imagination Time. Wednesday. 

**Community, Content, Marketing**

- Monthly Newsletter

  - Substack.
  - What to include? 

- Twitter

  - Zebu Articles.
  - Strategies

    - Tweets guiding you to do something. 
    - Responses to things going on in the industry. 
    - Tweets for engagement or funnels users to somewhere. 
    - Calls to action. Questions. 

- How to communicate our What token next vote?

  - rjkillmex review. 
  - Tweet. 
  - Poll. Our users thought this, what do you guys think? Twitter. 

- LinkedIn campaign. 
- Governance for the NFT space angle.
- Data Analytics. 

  - Potential smart contract read-through video. 
  - Potential. Writeup explainer. Or, FAQ pinned message.  

**BizDev**

- FVT Benefit Matrix almost near completion. 

  - Clean copy for Zebu. Naomi. 
  - Put into documentation. Marta. 

