Weekly Updates: July 5th, 2021
******************************

**Influence.vote**

- Contract made more generalizable. 
- Scaling stress tests completed. 
- Adding another layer of privacy and scalability over time using zero knowledge proofs. 
- Fixing bugs. 
- Need some designs for NFTs. Auto-generate. Add to IPFS. Will be compatible with OpenSea and others. 
- Build Influence narrative more. 
- Set up test for minting NFT identities and voting.
- Discussion on Proposal.vote.

**Yield.vote**
 
- Now in a new pulse. 
- Discussion on pool upgrade. 

  - Keep it rolling till the next governance decision. 
  - Aim for a governance decision in this new pool before the next pulse. Basic, Tycoon, & Resident Pools. 

    - What to do next? Data Review (csv).

  - ACTION: Get Resident Pool data csv. Chris.

- Designs

  - Building skyscrapers for Tycoon Pool.  

- Resident Pool

  - Fixing a failure issue bug. Chris. 
  - Audit number of failure transactions. 

**Governance**

- ACTION: Make To Do List before Nick’s Departure. 

  - Legal. 
  - Website Content. 

- Hiring

 - Suggestions from Matt, Tech Recruiter. 

   - Possibly incorporate OKR (Objectives & Key Results)? Into Agenda?
   - Suggestion: Create podcast. Matt assists.
 
 - Put a deal together for potential engineer hire.

**Community, Content, Marketing**

- “DAO Week” -- Mini Conference on Discord.  *(A reflective moment after the Obelisk phase and an opportunity to plan the future).*

  - Aim for July 14th, 15th, 16th.
  - ACTION: Finalize Program by Wednesday. Nick.
  - Promotion

    - We are leaders in Governance. 
    - Transparency and openness.

  - Generate content in the next 3 days. 
  - ACTION: Promo Video. Chris C.
  - Pre-Recorded videos. 

    - Review. What’s happened so far?
    - Where are we going next? Next phase.

  - Workshop sessions.  
  - Panel discussions / Round tables. Record. 
  - Content generated from Conference can be used for podcasts, tweets, etc. 
  - Create more vibe in Discord. 
  - Possible: Note minutes for ReadTheDocs (https://financevote.readthedocs.io/). Naved.
  - Invited speakers for Discord. 
  - Construct some documentation on each dApp while their respective sessions are run during the Conference. 
  - Zebu to promote the program. Will run a trafficking campaign. 
  - Come up with activities such as a meme competition. Prizes. 
  - ACTION: Announcement by Wednesday. 

- Discussion. Holding open strategy calls on Discord. Certain operations conversations to be done in public. Radical transparency. 
- Continue to move more conversations to Discord. 
- Upcoming meeting with potential influencer to engage. 
- Interview video with Chris.

  - Now edited. 
  - Use for Influence messaging. 
  - How to move video forward.

- New person to help with website content. 
- Possibility of beginning a Podcast. 

  - Record during conference. 
  - Variety of styles of episodes. 
  - Making content consistently. 

- Branding meeting. Tuesday. 




