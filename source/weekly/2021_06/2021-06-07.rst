Weekly Updates: June 7th, 2021
******************************
**NFT Auctions**

- Contract read through video for the NFT Auction contract recorded, although updates to contract missing and AV not great. Re-record? 
- Discussion on contract coding and architecture.
- Some auction edge cases such as no bid cancelation, resolved. 
- Discussion on setting up various functions for chaining auctions and modifying revenue share. 
- Live Auction: IRL NFT auction to happen in Bali, Indonesia  

  - Ongoing discussions with S21 Bali Crypto. 
  - Discussion on event logistics. 
  - Discussion of possible dates.
  - Aim for July 11th.

- Auction Zero (Within community, test)

  - What to be auctioned?
  - How to hype? What’s our strategy?
  - Possible dates. Aim for July 4th.
  - How to put out a call for memes, NFTs.
  - Start campaign soon -- Theo, Ksenya.
  - Product meeting for Thursday.

- Auction Main Net Launch

  - Aim for August 7th. 
  - Create memes/art about crypto and crypto history…. Meta-crypto.

- Discussion on asset passport for delivering physical NFTs.
- Possible test? Rollot plans.
- Contracts passed to Halborn for audit.

**Bank.vote and Airdrop**

- Plans moving forward. Product finalisation meeting scheduled for this week.
- Initiate: Operation: Join the Party.
- Vote Mining token economics close to completion.
- Land launch date for Bank 2.0 this week.

**Influence.vote**

- LDN21

  - Pay out rewards [DONE].
  - Possible next vote: What issue should be talked about next? Send out emails.
  - ACTION: LDN21 Debrief - Record assembly video.
  - Journalist article to zebu for content.

- Debrief from Priority Vote
  
  - Schedule Discord voice meeting to discuss the priority votes.
  - “You said, we did” equivalent.

- ACTION: Plan the “What next” votes?
- Decoding DeFi Votes.
- ACTION: Admin Panel Flow for Setting up votes. Internal Team Use.

**Yield.vote**

- Resident Pool

  - Is making revenue in fees. LP burn rates increasing.
  - New contract upgrade to “Pool Factory” in the process. 
  - Resident Pool debrief after this pulse. 
  - Wind up this pool, setup vote for the next set of parameters for the next pulse. 
  - Factory contract upgrade. Push a new contract on the next pulse with new stability and bug fixes. 

- Wrapping up resident, tycoon, itrust, and olyseum pools to then be upgraded. 
- Payout transaction failure issue. [Transactions Reviewed, Payouts today]
- Working on an issue with our ethtrader banner.
- Build analysis capacity for data tear down of resident pool data. Lots of research potential.

**Markets.vote**

- 2% of supply burned to mint Identities.
- Discussion of possibly creating separate market vote for those citizens with high V power (pro-market).
- What token next markets possible. Roadmap to deployment.
- Market rounds completed: BSC Ballot #10 and ETH Ballot #29.

**Merkle Drops**

- NFT merkle drop

  - Need to declare “commitment” via merkle proof and claim via merkle proof.
  - Influence identity screen airdrop integration.

- Ethtrader collaboration open.

**Operations**

- Litepaper first draft completed. Team review this week.

**Governance**

- Calls being put out for UX/UI designers. More design capacity is highly desirable.
- Meeting with potential technical recruiter.
- Pyramid Phase begun?
- Discussion on whether it’s time to open source the governance framework.
- Ongoing discussions with possible designer.

**Community, Content, Marketing**

- Deep Dive AMA June 4th: https://www.twitch.tv/videos/1045635014 
- Do another DD AMA on Friday. 

  - Advertise soon.
  - Collect questions throughout the week. 

- Create schedule/calendar for Discord.
- Biz Dev template constructed. 8-9 potential partners listed.
- NFT Deck and dApp suite deck content created.
