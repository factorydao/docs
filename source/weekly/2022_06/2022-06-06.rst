Weekly Updates: June 6th, 2022
******************************

**DAO Suite**

- **yield.vote**

  - Discussion on Stake-Weighted Basic Pool.
  - Pool around 25% full. 
  - Hold discussion with the community. Twitter Spaces. Vote? 
  
    - How was your experience with Yield? 
    - Should we do a second pool? 
  
  - Look into economic factors. FVT earned might not cover gas given the market falling. Gas analysis. Lizzl
  - Preparations for Avax Yield incentivized test. 

- **markets.vote**

  - Markets to polygon… What basket of tokens opens up markets?
  - Spend some FVT in order to play. 
  - New generative ID series for poly.
  - Soulbound tokens and the story of markets dApp. We were ahead of the game.
  - We have proof of market alpha from our prediction markets. 
  - Our goal is to work together to beat to the market.
  - Signal beats market, you get rewarded more. 
  - Play-to-earn. 
  - Doing a mixed basket this time -- Where money is moving on a global scale. Prediction market on money flow. 
  - Markets for crypto, forex (dollar, euro, pound, etc.) and equities and a mixed one. Maybe commodities.
  - Create vote proposals.
  - Building out leader board, reputation, who am I playing against.

- **mint.vote**

  - Get Mint on Polygon.

- Updates on Avalanche Incentivized tests. 

**Communications & Marketing**

- Markets mixed basket
  
  - Write vote proposals for mixed baskets.
  - Why is it exciting to vote on this?
  - We got a soul bound token prediction market. 
  - Activate OG members and they share their story/experience of markets.  
  - Get bullet points of history.
  - Video timeline, Chris C.
  - Social media, Substack.
  - Lizzl’s research on markets. Other analysis on markets. 
  - Announcements for "Return of the Markets" session. 

- Write announcements for Yield incentivized test on Avalanche. Publish Substack guidelines. 
- Work on some kind of scheduler for social media posts.
- Article on Soulbound tokens. Nick. 

**Community**

- Community Report. Every 2 weeks.  Ask DAO Leaders to make submissions in new submission realm. 
- "Return of the Markets" call this Friday. Discord.

**Governance**

- Legra -- Wednesday team meeting to setup and get going on Legra app. 

