.. finance.vote documentation master file, created by
   sphinx-quickstart on Mon Jun 28 11:51:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to finance.vote's documentation!
========================================

.. image:: ./images/financevote_dark_main.png
   :height: 100px
   :alt: docs-finance missing
   :align: center
   :target: https://www.finance.vote/

|
| finance.vote is a decentralised autonomous organisation (DAO) that builds software (smart contracts and browser interfaces). This software enables users to easily generate a token economy and  govern themselves as a DAO. Members of finance.vote use this software and the utility token FVT (finance.vote Token) to operate and govern finance.vote.
| We believe in Truth, Knowledge, and Imagination and will seek democratic outcomes to becoming an important digital creative institution in the future.

Our products
===============
The finance.vote dApp suite comprises decentralised applications that allow Web3.0 projects to create a token economy and digital democracy.

.. |A| image:: ./images/launch_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/launch/en/latest/
   
.. |B| image:: ./images/bank_icon_dark.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |I| image:: ./images/influence_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |M| image:: ./images/markets_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |MI| image:: ./images/mint_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/mint/en/latest/ 
   
.. |Y| image:: ./images/yield_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/yield/en/latest/

* |A| `launch <https://financevote.readthedocs.io/projects/launch/en/latest/>`_
* |B| `bank <https://financevote.readthedocs.io/projects/bank/en/latest/>`_
* |I| `influence <https://financevote.readthedocs.io/projects/influence/en/latest/>`_
* |M| `markets <https://financevote.readthedocs.io/projects/markets/en/latest/>`_
* |MI| `mint <https://financevote.readthedocs.io/projects/mint/en/latest/>`_
* |Y| `yield <https://financevote.readthedocs.io/projects/yield/en/latest/>`_

.. toctree::
   :maxdepth: 1
   :caption: Finance.vote:
   :glob:
   
   /content/finance_vote/*
   /content/FVT
   /content/tools
   weekly


.. toctree::
   :maxdepth: 1
   :caption: Whitepaper:
   :glob:

   /content/whitepaper/*

.. toctree::
   :maxdepth: 1
   :caption: Knowledge:
   :glob:

   /content/terminology/blockchain
   /content/terminology/proofs
   /content/terminology/networks
   /content/terminology/merkle_tree
   /content/terminology/yield_farming
   /content/terminology/liquidity
   /content/terminology/open_source
   /content/terminology/bitcoin_paper

.. toctree::
   :maxdepth: 1
   :caption: Analytics:
   :glob:

   /content/analytics/*
  

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
