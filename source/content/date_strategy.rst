The Data Strategy 

Contexts
Presentation
Business logic
Persistence
Blockchain

Dapp
Source
Map (state transition)
Reduce (conditions)
Sink

Data

CSV
Digested vote
Digested voice credit
MySQL
Influence Proposal Vote
Influence Proposals
$I balances
FVT balances
Ethereum (and clones) Blockchain
Primary vote
Primary $V
Primary FVT token
Primary Identity
Investor vault
LP Token vault
Team vault
Auction
Chainlink
Yield

Balances (derived from FVT)
I
V
FVT (not withdrawn)

Stats
log(% change) * trading volume, to weed out these 1000000% gains on $100

Data Structure 
Schema
Variable Names
Languages 

Purpose
Early discovery of tokens (notifications of scams)
Early dashboard.vote specification
Navigating data for engineering/design/marketing

Market Cap
Circulating Supply Data 
Team/Investor Tokens
LP Vault

Vote Data 
Two existing scripts are available to generate vote data and voter ranks.
Voice credits
VoterID
Owner
Data Science Layer 

Contract Token Supplies 

Trollbox
Ethereum
BSC
Bridge
BSC
Yield
Most Basic Yield 
Residents Pool
Tycoon Pool
Influence
Vote Mining 
VoteID IOU
Merkle drop (Bank)
TokenState


Node Infrastructure

Graph

Subgraph 


Information Strategy [FORK THIS]

Presentation

Gitbook



Documentation

Practices
Handling 
