Infrastructure
===============
 - Data (Python Pandas)
 - Visualisation (Chart.js, Neo4j)
 - Smart Contracts (Solidity, Web3.js, Infura, Ankr, Pokt)
 - Testing (Truffle, Ganache, Mocha, Cypress.io)
 - Wallet (MetaMask, Wallet Connect)
 - Repositories (GitLab)
 - CI/CD (GitLab, Docker, Puppeteer, Fleek, IPFS, Piñata)
