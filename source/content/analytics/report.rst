A report on the Vote Markets 
=============================
September 2021

-----------------------------

| **An analysis by @LIzzl Datadeo#8507**. For original analytics see :download:`this pdf file <../../images/analytics/FV_vote_markets_analysis.pdf>`.
|
| Users have voted over 37 rounds to predict the market performance of Defi tokens using a new voting mechanism called Quadratic Voting.
|
| This report analyses the information that was compiled from those votes. The main questions answered are:

    - How did users interact with the platform?
    - How did voters apply Quadratic Voting?
    - What makes a user a successful predictor?
    - What is the predictive power of markets.vote?

What is Quadratic Voting?
--------------------------
| That's a voting strategy that helps balance the voting power of big players and small ones. The idea standing behind QV mechanism is to make each additional vote cost a lot more than the previous one. 
| It's worth to remember that in QV participants vote for or against an issue, but also express how strongly they feel about it.
| The table down below depicts how cost in tokens depends on the number of votes:

================== ================== 
  Number of votes   Cost in Tokens
================== ================== 
    1                  1
    2                  4
    3                  9
    4                  16
    5                  25
    6                  36
    7                  49
    8                  64
    9                  81
    10                 100
================== ================== 

| More on quadratic voting: `click here <https://financevote.readthedocs.io/en/latest/content/whitepaper/19_token_economics.html#quadratic-voting>`_ or `here <https://financevote.readthedocs.io/en/latest/content/whitepaper/14_vote_market.html#quadratic-voting>`_.

How does the markets.vote work?
--------------------------------
| Users are incentivized to make market predictions in a series of tournaments focussed on a basket of crypto assets.
|
| Quadratic voting is used to generate a consensus in a perceived future market order. Users get a default voting power of 100$V. They then spend a budget of voting power to create a new order, based on their perception of token quality and future potential market performance.
|
| Users are rewarded with a proportional share of a network-generated reward pool depending on the proportionality of their correctness.
|
| Users can amplify their voting power beyond the starting level by demonstrating a history of correct decision-making in the markets, or by purchasing more identities.
|
| More on markets.vote in `the whitepaper <https://financevote.readthedocs.io/en/latest/content/whitepaper/14_vote_market.html>`_. Check out also `markets.vote app <https://marketsdotvote.eth.link/#/vote/how-it-works>`_.

Voter Activity
---------------

+------------------------+-----------------+
| How did users interact with vote markets?| 
+========================+=================+
| Total voters           | 298             |
+------------------------+-----------------+
| Total votes            | 1379            |
+------------------------+-----------------+
| Total voterIDs         | 597             |
+------------------------+-----------------+
| Average votes per round| 38              |
+------------------------+-----------------+
| Average user would mint| 2 voterIDs      |
+------------------------+-----------------+
| No voterID has ever been transfered      |
+------------------------+-----------------+


..  image:: ../../images/analytics/FV_share8.jpg

..  image:: ../../images/analytics/FV_round9.jpg


Voter turnout
---------------

..  |voter turnout| image:: ../../images/analytics/FV_voterturnout10.jpg
    :width: 700px



.. container:: box
    
    .. container:: left
        
        |voter turnout|   
        
    .. container:: right
        
        Voter turnout with **transaction cost***

        * (*average amount of ETH spent per transaction)*


How did voter apply QV?
------------------------
..  |hands| image:: ../../images/analytics/FV_votingpower12.png
    :width: 350px

.. container:: box
    
    .. container:: left
        
        |hands|   
        
    .. container:: right
        
        On average, voters used **72%** of their voting power per vote. 

        **100, 125, 97** were three most used amounts of voting power.


        On average, voters made **5** choices per vote. 

        With **10, 4, 1** being the most used number of choices.

|
|
|

.. image:: ../../images/analytics/FV_weightcombination14.jpg

Coin clairvoyants
-----------------

+------------------------------------------------------------------------------------------+
| Top 3 wallets with most Voting Power                                                     |
+==============+=======================+=======================+===========================+
| Address      | 0x34...39EA           | 0x6e...dDec           | 0x67...e640               |
+--------------+-----------------------+-----------------------+---------------------------+
| Voting Power | 5400                  | 4406                  | 3501                      |
+--------------+-----------------------+-----------------------+---------------------------+
| # of voterID | 50                    | 5                     | 15                        |
+--------------+-----------------------+-----------------------+---------------------------+
| # of rounds  | 3                     | 26                    | 16                        |
| participated |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| % of voting  | 95.9%                 | 45.9%                 | 61.3%                     |
| power used   |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| AVG # of     | 4.1                   | 3.7                   | 9.9                       |
| choices      |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| 3 most weight| [5,5,5,5]             | [10,10]               | [4,4,5,4,4,4,4,4,4,4]     |
|              +-----------------------+-----------------------+---------------------------+
| combination  | [5,5,5,5,5]           | [12,12]               | [4,4,4,4,4,4,4,4,4,4]     |
|              +-----------------------+-----------------------+---------------------------+
| used         |                       | [10]                  | [3,3,4,3,3,3,3,3,3,3]     |
+--------------+-----------------------+-----------------------+---------------------------+


+------------------------------------------------------+
| Top 3 Voting Power Earners                           |
+===============+============+============+============+
| Address       | 0x6e...dDec| 0xDc...CA7D| 0xB3...6B7C|
+---------------+------------+------------+------------+
| Bought Voting | 500        | 200        | 100        |
|               +            +            +            +
| Power         |            |            |            |
+---------------+------------+------------+------------+
| Earned Voting | 3906       | 1074       | 469        |
|               +            +            +            +
| Power         |            |            |            |
+---------------+------------+------------+------------+
| % Increase    | 781.2%     | 537%       | 375%       |
+---------------+------------+------------+------------+


+------------------------------------------------------------------------------------------+
|  How did the most successful voters used the vote markets?                               |
+==============+=======================+=======================+===========================+
| # of rounds  | 26                    | 25                    | 20                        |
| participated |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| % of voting  | 45.9%                 | 47.5%                 | 45.3%                     |
| power used   |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| AVG # of     | 3.7                   | 5.8                   | 9.7                       |
| choices      |                       |                       |                           |
+--------------+-----------------------+-----------------------+---------------------------+
| 3 most weight| [10,10]               | [5,5,5,5]             |                           |
|              +-----------------------+-----------------------+                           +
| combination  | [12,12]               | [+48 DIFFERENT        | [EACH VOTE A DIFFERENT    |
|              +-----------------------+                       +                           +
| used         | [10]                  | COMBINATIONS]         | COMBINATION]              |
+--------------+-----------------------+-----------------------+---------------------------+

Market predictions
------------------

.. |coins| image:: ../../images/analytics/FV_coins21.png
    :width: 280px

.. container:: box
    
    .. container:: left
        
        |coins|   
        
    .. container:: right

        **6** out of **30** coins were predicted successfully.
        Successful predictions were made in rounds **3,7,12,14,24,28**.
        
        There was no particular coin that was predicted more successfully than others. 
