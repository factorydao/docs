Tools used in our system
==========================
.. contents::
    :depth: 2

IPFS
-----------
*IPFS is a distributed system for storing and accessing files, websites, applications, and data.
IPFS is a peer-to-peer (p2p) storage network. Content is accessible through peers located anywhere in the world, that might relay information, store it, or do both. IPFS knows how to find what you ask for using its content address rather than its location.*

**IPFS Documentation:** `>IPFS<`_

.. _>IPFS<: https://docs.ipfs.io/



Ganache CLI
------------
| *Ganache is a personal blockchain for rapid Ethereum and Corda distributed application development. You can use Ganache across the entire development cycle; enabling you to develop, deploy, and test your dApps in a safe and deterministic environment.*
| *Ganache CLI(formerly known as the TestRPC) is the command line version of Ganache.*
| *Ganache CLI uses ethereumjs to simulate full client behavior and make developing Ethereum applications faster, easier, and safer. It also includes all popular RPC functions and features (like events) and can be run deterministically to make development a breeze.*

**Ganache CLI Readme for command-line documentation:** `>Ganache CLI<`_

.. _>Ganache CLI<: https://github.com/trufflesuite/ganache-cli/blob/master/README.md

Installation::

    npm install -g ganache-cli

    or

    yarn global add ganache-cli


Truffle Suite
--------------
*Truffle is a development environment, testing framework and asset pipeline for blockchains using the Ethereum Virtual Machine (EVM). It is based on Ethereum Blockchain and is designed to facilitate the smooth and seamless development of DApps. With Truffle, you can compile and deploy Smart Contracts, inject them into web apps, and also develop front-end for DApps. Today, Truffle is one of the most widely used IDEs for Ethereum Blockchain.*

**Truffle documentation:** `>Truffle<`_

.. _>Truffle<: https://www.trufflesuite.com/docs/truffle/overview#truffle-overview

Installation::

    npm install -g truffle

Requirements::

    NodeJS v8.9.4 or later


| * Recommendations for Windows
| If you're running Truffle on Windows, you may encounter some naming conflicts that could prevent Truffle from executing properly. Please see the truffle documentation section on resolving naming conflicts for solutions: `>Here<`_ .

.. _>Here<: https://www.trufflesuite.com/docs/truffle/reference/configuration#resolving-naming-conflicts-on-windows