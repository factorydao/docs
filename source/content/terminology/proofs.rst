Consensus Algorithms
======================
| Within a blockchain network there are several actions that can be undertaken and every one of them has to be validated. But what does that mean exactly?
| 
| Validation within a blockchain network is based on proofs which are widely known as **Consensus Algorithms**. Consensus stands behind achieving agreement on such matters as confirming transactions or producing new blocks on a chain. Whenever one of those actions are requested, a consensus protocol is triggered. To reach a consensus, a majority of nodes have to agree on something. People often reach a consensus easily by voting on something, and when a majority of votes are the same, a consensus is reached. In the computer world, things are a bit different since voting would be too vulnerable to attack and too easy to be tampered. That's why consensus algorithms were brought into blockchain technology.

| There are plenty of different consensus algorithms. Some of the most common ones include Proof of Work, Proof of Authority, Proof of Stake, and Proof of Burn. 
|
| Right now, Proof of Work is the most commonly used consensus algorithm, but Ethereum has plans to use Proof of Stake in the future. It's worth mentioning that Proof of Work (PoS) and Proof of Stake (PoS) are the two best known sybil deterrence mechanisms. This is why they are the most common mechanisms within the cryptocurrency world. 



Proof of Work
--------------
| Given its name, understanding this algorithm is fairly straightforward - you've got to prove that you've done some work. But how do you exactly prove you've done the work?
|
| Proof of Work involves all the nodes within a network doing some work, and that means every computer has to take part in solving a complex mathematical problem. In simpler words, work means computer calculations.
|
| As mentioned before though, these mathematical problems to solve aren't based on the mathematical methods we know from school. Solving one can be done only by trial and error, and the odds of solving the problem are about 1 in 5.9 trillion. This requires a lot of computing power that leads to large energy consumption. Solving the problem is known as mining and nodes taking part in it are miners. So to encourage miners to mine, this huge consumption of energy must be profitable. And it is.
|
| Because one computer would be solving this kind of problem for years, a whole chain is working together but not every node will win. The winner is the first node that can solve the problem. This node then shares the requested action to the chain and gains freshly minted currency as a reward. Mining allows you to earn cryptocurrency without buying it.
|
| To understand that better, let's imagine a mine. To begin with, a miner doesn't know where the gems are located. Working alone would require vast amounts of time, digging one spot at a time and having no certainty of finding anyting. With the help of a team though, mining would go much faster as a bunch of people would be digging multiple areas at the same time. The only difference is that in crypto mining, the gem or mining reward of freshly minted cryptocurrenty is awarded to the first miner who solves the problem.
|
| Speaking more technically, let's first establish that **the cryptocurrency network sets a target hash**. We'll need this information later.
| The mathematical problem to solve can be found in block's header, which contains:

* the block version number
* a timestamp
* the hash used in the previous block
* the hash of the Merkle Root
* the nonce
* the target hash

| All the above creates a block hash, which in PoW each node takes and calculates. Since a given set of data can only generate one hash, miners alter the input by adding an integer called a nonce. When one node reaches the expected calculated value (finds the correct nonce), it will broadcast the block to the other nodes and the block will be added to the chain when a majority of nodes confirm the correctnesss of the hast value. 
|
| **What is a target hash?**
| 
| A target hash is a numeric value that is used to determine how hard it is for the miners to solve the block.
|
| **What is a nonce?**
| 
| A nonce is an abbreviation for "number only used once". It is one of the block header components and the number that blockchain miners are trying to solve. The nonce is the value that miners can alter to create different permutations and generate a correct hash.
|
| The purpose of proof-of-work algorithms is to deter the manipulation of data. Unfortunately, these algorithms use large amounts of energy in order to run such enormous computations. This has raised concerns over mining's ecological footprint. Overtime though, mining will continue to trend towards greener solutions as they reduce energy costs and thus increase miners' profits. 

Proof of Stake
--------------------
| In contrast to proof-of-work algorithms, proof-of-stake algorithms are less energy intensive, which is one of the arguments for switching entirely to a PoS in the future. It is energy-saving because it doesn't require thousands of nodes working together on finding the nonce. Instead, the algorithm randomly assigns one node to add a block to the chain and assigns several other nodes to confirm the addition. 
|
| What's the catch?
|
| To become a proof-of-stake algorithm validator, a user must stake some cryptocurrency of the very blockchain that it is validating.  For the Ethereum network, the staking amount is 32 ETH that can be provided by one user or by a pool of users. Such staking is both a kind of collateral and incentive. Naturally users don't want to lose their money, so staking funds into a pool makes users want to protect it from destruction, which is the consequence of validating a malicious block. 
|
| Proof of stake doesn't make validators compete, so only a chosen node, known as "the main validator", works to create a block. Then, an attesting group validates this proposed block and decides whether the block is malicious or not. Again, every node attesting a block either takes credit for a good attestation or suffers the consequences of a bad one. 
|
| For a deeper insight into this topic, checkout `ethereum docs <https://ethereum.org/en/developers/docs/consensus-mechanisms/pos/>`_.
