Semantic Ballot Voting
===========================
.. |ballot| image:: ../../images/ballot.jpg
    :height: 50px
    

| Semantic Ballot Voting is a new kind of voting system designed specifically for finance.vote. It utilises a stack of semantic tags (in this case token tickers for tradable cryptocurrencies) and quadratic voting [7]_.
|
| Users are presented with a market ordered list and are requested to use their vote power tokens ($V) to vote on the cryptocurrencies of their choice. They convert $V into votes, by quadratic voting:

    |ballot|

| Meaning that every subsequent vote has a non-linear cost.
|
| This system enforces prioritisation and ensures that users cannot vote strongly on every item, an issue that decreases validity in conventional voting and surveying systems. This scheme is used repeatedly throughout the finance.vote ecosystem and helps build weighted consensus across the whole network.




.. [7] “Quadratic Voting: How Mechanism Design Can ... - SSRN.” 13 Feb. 2012, https://www.ssrn.com/abstract=2003531. *Accessed 26 Aug. 2020.*