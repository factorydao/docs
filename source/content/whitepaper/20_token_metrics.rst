Token Metrics
===============
| **The $FVT token has an initial generation amount of 1 billion tokens (1,000,000,000 $FVT).**
|
| These tokens are split into a set of tranches as follows:

Initial Distribution
--------------------
| **20% of the tokens (200,000,000 $FVT)** are to be distributed to early adopting participants through a series of distribution rounds.
|
| The first wave have concluded and took the form of two private rounds: 
| **Seed 6% (60,000,000 $FVT)** tokens @ $0.007
| **Private 12% (120,000,000 $FVT)** tokens @ $0.008
|
| Those participants that obtain tokens in these rounds are subjected to cliff-linear vesting over a period of 5 months.
|
| The final 2% of these tokens (20,000,000 $FVT) will be distributed by a decentralised auction mechanism (auction.vote), which will go on to form a key component of the finance. vote ecosystem (details of this mechanism will be released in a standalone post).

Team and Advisor Stakes
------------------------
| **15% of the tokens** are allocated to the founders of the network. These are released to the team using (6,3) year cliff-linear vesting i.e. vested for three years, with a 6 month cliff.
|
| **5% of the tokens** are allocated to valuable strategic advisors. These are released individually to advisors using (6,2) year cliff-linear vesting.

Governance Incentive
---------------------
| **10% of the tokens** are to be distributed to voters of the system over a (6,5) cliff-linear vesting schedule. These tokens will be distributed to users proportionally to the number of $V spent in the system associated with their identities. We call this process vote mining and it is one of a number of mechanisms designed to break voter apathy in blockchain governance.

.. image:: ../../images/vesting_schedules.png
    :height: 400px
    :align: center

The DMF Treasury
------------------
| **30% of the tokens (300,000,000 $FVT)** will be distributed through (20,5) step vesting i.e. 20 tranches of 15,000,000 $FVT, in a lump sum for 5 years to the DMF treasury.
|
| DMF funding tranches will be distributed via quadratic funding mechanisms to stakeholders who bid to do work for the network. If the DMF participants choose, a proportion of each tranche can be burned, providing further control of the monetary policy.

.. image:: ../../images/dmf_treasury.png
    :height: 400px
    :align: center

Liquidity Pool
---------------
| 20% of the tokens (200,000,000 $FVT) will be utilised to bootstrap the liquidity of the finance.vote network.
|
| The liquidity pool is split as follows:
|
| 2% of the tokens (20,000,000 $FVT) will be utilised as match liquidity in the initial distribution auction.
|
| A minimum of 9% (90,000,000 $FVT) are allocated to liquidity miners in the DEX space. These will be distributed via a pulsed liquidity mining incentive scheme to holders of LP tokens in respective decentralised exchange pools.
|
| 9% of the tokens (90,000,000 $FVT) will be utilised to engage with the centralised exchange (CEX) space. These are unlocked from network launch, but will be utilised transparently so that users can understand monetary flows in the system.

.. image:: ../../images/liquidity_mining.png
    :height: 400px
    :align: center

Circulating Supply
-------------------
The aggregated emission from these schedules provides us with the following emission curve represented as a percentage of total genesis supply.

.. image:: ../../images/circulating_suply.png
    :height: 400px
    :align: center

Inflationary Mechanisms
-------------------------
The finance.vote core contracts hold the power to mint new tokens beyond the 1,000,000,000 $FVT at genesis. This is chosen to ensure that there is an open ended solution to incentivising adoption. In the first instance a small amount of additional inflation is used to seed liquidity for reward pools in our vote markets, starting @ 100,000 $FVT per week / per market.

Deflationary Mechanisms
------------------------
In order for any user to take part in the system, they must first obtain a Digital Identity Token (DIT). In order to do this, users must obtain $FVT and burn at least 100 to take part. This introduces an adoption based deflationary dynamic to the system.

Emission Schedules and Monetary Policy
=======================================
| The emission schedule of a token system determines the inflation rate of a token economy.
|
| It has recently become the trend to release entire token supplies in a matter months if not weeks in the recent DeFi boom. This has led to hyperinflation and short lived token economies.
|
| We are not aiming for a multi generation store of value system, neither are we claiming to be “money” (not yet anyway). The Finance Vote Token ($FVT) is a utility token, which provides users with access to a governance system.
|
| The governance system acts as a kind of crypto economic hub, which the users will ultimately control. $FVT is required to access the system, without it you will not be able to vote or participate in the governance decisions.

Cliff-linear Vesting
---------------------------
| Cliff-linear vesting is a smart contract based vesting schedule that we have decided to add confidence to emission schedules.
|
| The greatest threat to a token economy is a highly asymmetric token distribution, even worse if it is an unknown token distribution.
|
| We have programmed a schedule, which releases a small proportion of tokens to network stakeholders in a lump sum, followed by a block-by-block distribution for a prolonged period of time.
|
| It follows that:
| **Token Amount = (month of cliff from genesis, vesting period length in years)**

.. image:: ../../images/ta.png
    :height: 50px
    :align: center

Vote Mining
--------------
| A 100,000,000 $FVT(6,5) allocation has been granted to all voters in the system. This means that approximately 9,917,808 $FVT will be airdropped to voters 6 months after genesis and then continually at the same rate of 54,794 $FVT per day for 5 years.
|
| This allocation will be steered by the community throughout the 5 year vesting period. At the moment it is only possible to vote in our vote markets, but eventually vote mining rewards will be flowed to both our miniDAOs and the DMF to break voter apathy.

Pulsed Liquidity Incentives
----------------------------
| The provision of decentralised liquidity is a revolution. The dynamics of this activity are very early, however the dawn of the “agricultural revolution“ has introduced incentive dynamics to the liquidity provision process with demonstrable success.
|
| We have created a tuneable system that will ensure that we maintain effective capital efficiency of our trading pools for the lifecycle of the network development, aiming to optimise for trade volume, market depts, inflation cost through dynamic incentives.
|
| Our approach to this is through adding a cyclical liquidity dynamic, with tunable parameters, that include pulse height, quadratic decay rate and a pulse width.
|
| The result is a liquidity cycle producing periods where: yield is high, yield is low and a range of levels in between that drop quadratically between pulses.
|
| This provides LPs with short time horizon trading strategies with an opportunity to optimise their yield through complex farming techniques, or users with a longer term strategies deciding to stay the pool across cycles.
|
| The management of these parameters will be tuneable in the DMF and offer the possibility to minimize the impact of divergent loss by optimising for pool growth through governance activity.

.. image:: ../../images/1_year_pulsed.png
    :height: 400px
    :align: center

Summary
---------
The finance.vote token economy introduces a range of dynamic crypto economic monetary policy ideas with the view of creating a sustainable crypto economic hub. The system uses collective decision making and voting technology to create a participatory token economy driven by the desire to understand the cryptospace itself.