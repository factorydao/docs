Digital Identity Tokens
==============================
| Digital identity is a crucial component of voting technology. It is particularly important in quadratic voting systems, where if it is trivially easy to create multiple identities, then the quadratic system trends back towards being a linear system and honest actors are left at a disadvantage.
|
| The act of creating multiple identities with the intent of corrupting a system is known as a Sybil attack. This is an issue globally across many social systems including most legacy social media systems, such as Twitter and Facebook. Since it is obvious that narratives can be manipulated by controlling the frequency of certain hashtags, the incentives exist to manipulate social consensus through Sybil attacks. It’s the Sybil War. 
|
| In this increasingly adversarial context, all applications in the future will require a degree of Sybil resistance. This is typically obtained, mostly ineffectively, by collecting some piece of identifying information, such as a phone number, or more aggressively, state issued identity documentation. In permissionless systems, this is an unacceptable solution. 
|
| Finance.vote utilises a novel system we call Decentralised Identity Tokens (DITs). These take the form of ERC721 compatible NFTs, which represent an identity within the system. Users will not be able to vote in the system without one and cryptoeconomic dynamics are used to prevent the trivial creation of identities.

Decentralised Identity
-------------------------
The DITs in the finance.vote ecosystem contain the following information:

* The voting history of the identity.
* The reputation of the identity, denoted in voting power $V.
* An adoption metric, denoted by a number i.e. $FVT1 to $FVTn, where n is the issuance number.
* Metadata allowing customisation of the DIT look and feel.


A default skin for users’ will be generated in the Obelisk phase from procedurally generated art, producing a one of a kind artwork for each DIT. Certain numbers will have increased significance.

Identity Customisation
------------------------
| The finance.vote digital identity tokens start as an NFT that simply contains users voting histories and therefore performance within the market. In the Obelisk phase of the network these will gain their own procedurally generated provably unique digital art. However, they are customisable to users preference and are intended to be used as avatars in the system. 
|
| Users will be able to link their DIT with other NFTs they have purchased from a marketplace or from auction.vote, showcasing their art choices to other users on the platform.

If you can’t beat them, join them
----------------------------------
| In the finance.vote ecosystem, Sybil resistance takes the form of ensuring that users cannot corrupt the consensus outcome by splitting votes across multiple ballots and fabricating multiple identities. In reality, this cannot be entirely stopped.
|
| We therefore take an economic, pay-to-sybil mitigation approach, to reach a state where good intelligence on the number of Sybils in the system is known. Before a user can engage in voting activity on finance.vote, users must acquire a minimum of 100 $FVT, which will be sent to the identity minting address and burnt, which assigns voting rights to a user Ethereum address. Only addresses, where a DIT is present will be able to vote, addresses with multiple DITs will be able to vote multiple times.

A Cryptoeconomic Line of Defence
---------------------------------
| Using a similar mechanism to that found in the auction.vote system, the price of DIT will increase exponentially based on demand. If a user buys an identity for 100 $FVT the price will double to 200 $FVT for the block immediately after the buy. This price will then decay at a rate of 1 $FVT per block until it reaches the price floor of 100 $FVT, unless a subsequent buy occurs at a price delta between 200 $FVT and the price floor.
|
| All $FVT is burned on identity creation, ensuring that token holders benefit from adoption and Sybil activity.

An example buy sequence:

* The first user in the system purchases, FVT1 for 100 $FVT.
* Over the period of 100 blocks the price returns to 100 $FVT.
* FVT2 is bought for 150 $FVT and the price in the next block jumps to 300 $FVT, where the same user immediately purchases FVT3.
* The price is now 600 $FVT for an identity and users wait until the price decays until their perceived value of a DIT is reached.

.. image:: ../../images/whitepaper_voting.png
    :height: 600px
    :align: center

Tradability
------------
| finance.vote DITs will be tradable as are any other NFTs. Therefore, it is possible that users may be able to accrue reputation and therefore vote power on an identity and then realise that work and skill into a profit from their initial purchasing price.
|
| Users may also wish to trade identities for their aesthetic or perceived value based on their issuance number; lucky numbers are very real to some.

Decentralised Reputation
-------------------------
| Identities within the system are designed to allow users to accrue reputation based on their ability to make effective market predictions and influence consensus formation in the second layer governance system.
|
| These dynamics are difficult to Sybil. The nature of our chained voting system ensures that progressively correct accounts earn greater reputation and voting power over time and the likelihood that a user can successfully construct these accounts randomly as opposed to playing honestly diminishes over time.

Summary
-------------
Many presume that Sybil resistance can only be obtained through the use of hard identity solutions. We propose that pseudo-anonymous identity formation can occur in decentralised systems through the use of non-fungible tokens. We believe this will be an important piece in the evolution of DeFi and decentralised identity. 