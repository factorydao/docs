===========
Governance
===========
| The finance.vote governance architecture is intended to introduce a new paradigm of governance into the cryptospace, utilising both a layer one DAO, the Decentralized Monetary Fund (DMF) and the Layer two $FVT miniDAO in a bicameral governance structure.
|
| From early in the launch, users will be given the opportunity to materially shape the future of the network and progressively, layer one powers will be transitioned in their entirety to the token holders. $FVT will act as the governance token for the network alongside it’s incentive generating base utility properties as a cryptocurrency.

The Decentralised Monetary Fund (DMF)
=======================================
The DMF will build a novel DAO pattern optimised for control of the finance.vote monetary policy to the token holders. The DMF sets the inflation rate by creating new reward pools and initiating new voting markets. It also controls the ecosystem development fund, which will add full transparency to the assets designed to maximise the impact of finance.vote ecosystem within the crypto community.

The $FVT MiniDAO
===================
The $FVT MiniDAO will showcase the potential of second layer governance by developing the finance.vote ecosystem in partnership with the users. A series of feature votes will be launched, early in the Pyramid phase, which will allow users to vote on future tournaments and reward pool sizing. The $FVT MiniDAO is where policy is debated prior to ratification and material shifts in direction of the system occur.

The Path to Decentralisation
==============================
The finance.vote network leads with an ethos of maximal decentralisation. That is, we aim to disintermediate key network functionality throughout a phased deployment of the network, retaining control only where it is absolutely necessary.
The network will be a permissionless system, creating an inclusive space where ideas can be negotiated and digital asset price discovery can take place through decentralised voting techniques.

The network is deployed in a three phase path towards decentralisation.

Obelisk (From network genesis)
------------------------------
| This is the launch phase of the network during which a number of key components of the system are introduced to the users, including auction.vote and the vote markets.
|
| It will not be possible from the outset to build these complex human interface features, without some degrees of centralisation.
|
| In this phase a number of management keys exist that can, through human input, modify the rules of the game in such a way that they can be optimised for adoption based on feedback from the community.
|
| For example, in the pre-TGE phase. Digital Identity Tokens will be issued via management key to engaged community members for the purposes of bootstrapping adoption.
|
| Once the $FVT token is available, this process will be replaced by a purely permissionless perpetual auction system, whereby users burn $FVT to obtain DITs. This simple example cryptoeconomic disintermediation directly demonstrates the efficacy of utility tokens for facilitating decentralisation.
|
| Throughout this phase, we create and optimise adversarial cryptoeconomic games that will stress test the permissionless governance structures of the network.
|
| These include our voting technology, which utilises quadratic voting and therefore requires a sybil protection layer created by our identity system. Again, this will be tuned by human input, but once a clear parameter space for this component has been identified, it will transition to token holder vote.
|
| The voting system in this phase is turned towards the markets in an attempt to understand the context in which the network is launched. The launch team will use the data produced by this to build tools and infrastructure for education and collective intelligence building. Informed consensus is a progressive long term process and it will be a responsibility of the network launch team to support in community understanding and knowledge building.
|
| Throughout this phase the voting and auction systems will be optimised using our management keys so that we arrive at tunable parameters that can be transitioned to the main DAO, the DMF in phase 2. The launch team will aim to utilise a transformational leadership style, with a view towards showcasing effective practices for network maintenance and responding dynamically to token holder feedback. Our network culture is one of radical openness [16]_.


Pyramid (Begins 6 months post genesis, end Q2 2021)
----------------------------------------------------
| In this phase we launch our second layer governance system, which aims to further empower the community with decision making capability on the network.
|
| In this phase, control of key system parameters are transitioned to the DMF and at an appropriate time the vote market management keys will be burned.
|
| In this phase we use quadratic voting to turn dialogue into numbers.
|
| Here we aggregate rough consensus so that it can be lensed into coherent decision making. This is a complex iterated process that will begin in this phase and will become a foundational paradigm of the network.
| We do this in token specific miniDAOs. These are platforms where network level discussion is curated so that clear signals can be generated from the community.
|
| Here social self organisation is promoted so that new system parameters and functionality can be discovered. In this phase, the finance.vote network core team are still in service to the users and will design, develop and deploy core technologies for the network and will expand the community of developers and users through the use of the DMF treasury.
|
| Throughout the pyramid phase, the finance.vote reputation system will be tuned to ensure that a healthy mix of stake weighted and meritocratic consensus formation is used for key decision making. The system will be further hardened against sybil and collusion attacks [17]_.

Starship (Target 18 months post genesis)
-------------------------------------------
| In this phase, full responsibility for the network will be transferred to token holders.
|
| All three initiating components; the vote markets, second layer governance and social trading will be deployed to mainnet and operating effectively. Each of these will have tunable parameters, with a history of effective decentralised decision making, with a clear trend towards systematic optimisation.
|
| By this point there will have been a number of successful quadratic funding sequences utilising the DMF treasury and a growing decentralised development community will be building on the network with a new roadmap that is currently unknown to the launch team.
|
| In this phase the crowd will be in full control and all management keys will be burned. Responsibility for future development is now with the community and network leaders will be elected through decentralised direct democracy.

.. [16] “10 defining principles of radical openness - UNHCR Innovation.” 28 Jul. 2016, https://www.unhcr.org/innovation/radical-openness/. *Accessed 17 Nov. 2020.*
.. [17] “On Collusion.” 3 Apr. 2019, https://vitalik.ca/general/2019/04/03/collusion.html. *Accessed 17 Nov. 2020.*