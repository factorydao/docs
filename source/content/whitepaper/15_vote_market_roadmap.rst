The Vote Market Roadmap
==========================
| The vote market concept is vast in scope. We roadmap a number of vote markets as network leaders, however it is intended that the token holders and the DMF take over the responsibility of generating new vote market concepts in the future.

Launch Market - DeFi (Winner)
---------------------------------
| Our launch market is aimed at where the action is, the DeFi market.
|
| DeFi is the most rapidly growing and changing market on the planet. Innovation occurs at lightning speed and some products, the “DeFi Blue Chips”, can ship new contracts with great frequency, shifting fundamentals dramatically.
|
| This is the perfect context for testing the efficacy of the vote markets as an alpha generating system.

DeFi (Loser)
---------------------------------
| Just as DeFi products can ship new game changing contracts that add new decentralised financial primitives to the market. They can just as easily be compromised through an exploit resulting in the loss of user funds.
| These first two markets will be the capstone vote markets for the system.
| DeFi Vote Markets - Initial Token List Curation
| We begin by using the top ten tokens from the CMC DeFi token list.
|
| This list will be modified through deliberation on our social media channels and a rolling snapshot vote (in advance of our second layer governance system) will be held to determine token list inclusion. A maximum of one token per week will be added to the DeFi token list until a fully automated process is completed.

New Tokens
---------------------------------
| A vast amount of new tokens are listed every day across the decentralised exchange ecosystem. The outstanding majority of these tokens are valueless, or outright attempts to scam users of the cryptospace.
|
| The “New Tokens” vote markets will be designed to utilise the collective intelligence of the finance.vote user base to filter these tokens through a successive iterative league table system.

.. image:: ../../images/uniswap.png
    :height: 700px
    :align: center

| New tokens will be indiscriminately scraped from the uniswap contract ecosystem into a token list called “The Dumping Ground.” A top slice of these tokens will be added to the finance.votedivision system.
|
| In the division system. A nested layer of leagues will be used to to filter tokens via promotion and demotion until they hit the top of the division one vote market. These winning tokens willgraduate to more senior markets, such as the DeFi Winner market.

Thematic Token Lists
---------------------------------
As our token curation system matures, a proposal system will be introduced that allows users to compile thematic token lists related to tokens aligning to specific technological affordances eg. Layer 2 tokens, Privacy Tokens, Classic Alts etc.

REKT
---------------------------------
| The REKT market is a vote market based on predictions of token blow up. 
| This vote market will run until one of the tokens in the token list experiences a 90% drawdown in price from the initial market snapshot.
|
| This market will be utilised to detect crowd perception of stablecoin blow up risk, with a basket of tokens that span a range of market maturity. Which projects are most likely to beexploited? Which projects are likely to experience a governance attack, or catastrophic governance failure? Which are under existential threat?
|
| It is the goal of the Rekt market to collect this intelligence.

Experts
---------------------------------
| The “experts” vote market is for pre-release tokens.
| In this market, a group of experts are invited to take part in a vote and will be attributed special “Expert” voting rights.
|
| The experts will quiz token creators on their proposed future projects and the experts will vote with their identities to generate a collective expert vote. Simultaneously, users will votewith their identities, aiming to predict the outcome of the expert vote.
|
| The experts become the ‘oracle’ in this system and the vote market is settled on their vote.
|
| The outcome of this vote launches the successful token immediately in a liquidity bootstrapping event on auction.vote.

Memes
---------------------------------
Meme markets, the most difficult market of all. The generation of high fidelity meme voting will be highly complex and contested. Achieving this will require advanced blockchain governance.

Staking
=================================
| The vote markets will not stay as merely as a collective intelligence and consensus tool.
|

| More effective price discovery will arise from those with skin in the game. For those users who wish to increase their exposure to these market bets, a staking system will be released thatwill allow users to stake $FVT on the market prediction outcomes.
|

| Architecturally, the staking system will be a different layer from the voting system reward pool.
|

| Our first staking system will allow staking $FVT on an identity for a single round of a single vote market. This identity may or may not belong to the staker, therefore this will be thefoundation of social trading, betting on an identity that does not belong to you.
|

| In this formulation, every (round, tournament) pair will have a collection of stakes, up to a maximum of one for every identity. The set of identities for which there are non-empty stakes will essentially be in competition with each other, with the total $FVT stake gettingredistributed among the various stakes in proportion to how their identity voted on the correct outcome. In this game, the losers pay the winners.

.. image:: ../../images/staking.png
    :width: 300px
    :align: center

| For example, suppose there are 3 identities, Alice, Bob and Caroline, and they vote on the winning choice with votes 1, 3 and 0, respectively. Suppose further that the stakes on Alice, Bob and Caroline are staking 10, 20 and 30 $FVT, respectively.
|

| Then once the winner is announced, the total stake on that round (10 + 20 + 30 = 60 $FVT) would be distributed proportionally to the stakes, with 1 / (1 + 3 + 0) = 1 / 4 of the 60 = 15 going to the stake for Alice and 3 / (1 + 3 + 0) = 3 / 4 of the 60 = 45 going to the stake for Bob, and 0 going to the stake for Caroline. In this situation, the stake for Alice will have started at 10 and ended at 15, a 50% gain, and the stake for Bob will have started at 20 and moved to 45, a 125% gain. In essence, the losing bets are reflowed to the winners. Natural selection is beautiful.
|
| **10% of the losing stake will be burned in each round.**
|
| The staking system is still in the design phase, however is targeted for release in the Obelisk phase of the network. It will allow fair staking of multiple coins simultaneously, a first in both the cryptospace and the wider financial system, as far as we are aware.