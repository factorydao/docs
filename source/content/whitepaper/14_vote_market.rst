Vote Markets
==============
Introduction
-------------
| The finance.vote network leads with a quadratic voting based prediction market system we call “vote markets”.
|
| The system is designed as a crypto economic game that marries governance with the markets. It is designed to aggregate collective intelligence from a distributed group of pseudo-anonymous crypto users who have their finger on the pulse of the crypto market.
|
| Quadratic voting is used to generate a consensus in a perceived future market order. This is done by presenting each user with a token list that is by default ordered by the market. Users then spend a budget of vote power to create a new order, based on their perception of token quality and future potential market performance.
|
| The resultant aggregation of user ordered token lists creates a distribution of perceived market order in the form of a consensus list.
|
| At this point in the future, users are rewarded with a proportional share of a network generated reward pool depending on the proportionality of their correctness.
|
| This simple mechanic becomes exponentially more powerful as new and diverse markets are added and as the power of the rules of the game is transitioned to token holders.
|
| The purpose of this game is to create an adversarial environment to release and battle test our quadratic voting technology, including our decentralized identity token system so that it can be used to aggregate and curate collective intelligence and reach effective decisions in our second layer governance system.
|
| **The vote markets have the following properties:**

One Sided
-----------------------------------------------
| Prediction markets are limited by adoption and liquidity. Similar to the issues seen with long tail assets such as those found in emerging altcoin markets, when liquidity is thin price discovery becomes difficult.
|
| Prediction markets turn predictions into tradable assets. This is potentially profoundly disruptive and has the potential to create futarchy based DAO governance structures in the future. However, the fidelity of predictions are contingent on the number of players in the game and further, the depth of their order books.
| AMMs have opened up the long tail asset space by ensuring that there is always a trade available at a given price through algorithmic price discovery mechanisms. The pay off is slippage. They are one sided markets, a smart contract becomes the counter party.
|
| The finance.vote vote markets use a similar design philosophy by seeding liquidity using a reward pool system. Your counterparty is the network, rather than a trader. The result is a collective reward pool that is distributed programmatically to winning votes.
|
| In the same way that uniswap has eliminated order books from token trading, we eliminate order books from prediction systems.

A Vote / Market Window
-----------------------------------------------
| Each vote market comprises two time bounded windows, a vote window and a market window. In the vote window, users have a period of time to submit their votes, signalling their preferred market ordering, when this window closes, the market window opens. Here, no more votes are submittable and the predictions of voting users play out in the market. At the closing block of the market window, typically weekly at UTC market close, a snapshot of the top gaining token, is determined by a market oracle and is compared against the consensus state.

.. image:: ../../images/market_window.png
    :height: 200px
    :align: center

Token Rewards
-----------------------------------------------
| In the opening state of the system the reward pool is best conceptualised as an incentive for effective research. The cost of entry is the purchase of a decentralised identity token and the gas costs required to submit an on-chain vote utilising the vote market smart contracts.
|
| The user vote pool share is allocated as follows [8]_:

.. image:: ../../images/pool_vote_share.png
    :height: 600px
    :align: center

.. [8] Note that this is computed as votes, not $V, which are votes2, whereas the $V bonus is equal to the $V spent on the winning ticket.

Quadratic Voting
-----------------------------------------------
| Quadratic voting first proposed by Glen Weyl and Eric Posner [9]_ is a voting paradigm that is designed to allow users to vote more than once on a particular issue, moving beyond the 1 person, 1 vote (1p1v) paradigm conventional in most democratic systems. Instead, voters are allowed to buy more votes. The cost being that the more subsequent votes that are bought, the greater the cost by the square of the votes. This allows users, particularly those with minority views points to express their preference with more intensity than would be possible in a 1p1v system.

.. image:: ../../images/quadratic_voting.png
    :align: center

.. [9] “Voting Squared: Quadratic Voting in Democratic Politics” by ....” https://scholarship.law.vanderbilt.edu/vlr/vol68/iss2/3/. *Accessed 17 Nov. 2020.*

Meritocratic Reputation System
-----------------------------------------------
| In our initial markets, users can amplify their vote power beyond the starting level by demonstrating a history of correct decision making in the markets, or by purchasing more identities.
|
| Vote power in the system is denoted by the use of an internal vote power token $V. Every identity is airdropped a budget of vote power tokens to spend in each vote market, in every voting window, we call this “Power UBI”.
|
| Each user starts with a Power UBI of 100, giving them 100 $V to spend on votes in a vote market. The base UBI level for each identity increases based on their history of correctness.


| For example, if a user votes on $TokenX 6 times in a “Winner” vote market and $TokenX is the highest gaining token at the end of the market window. Then that identity has spent 36 $V on a winning prediction and will therefore receive a 136 $V Power UBI from that point on. There is no reduction in this user’s UBI after this point. The reputation system is built on non-punitive, positive reinforcement only.
|
| This ensures that over time vote power balances and therefore sizes of claim on the reward pool will trend towards users with a history of correctness in the markets.

.. image:: ../../images/winning_token.png
    :height: 200px
    :align: center

Minimal Cost of Entry
-----------------------------------------------
| Engaging in early altcoin markets is the best way to gain high risk / reward trades. It is also the best way to get rekt. With a heavy bias towards the latter.
|
| finance.vote offers users the opportunity to make market bets on the altcoin markets without purchasing and holding the hyper volatile underlying assets. Once users hold a voting identity they get a free bet in every vote market on the finance.vote network, outside of the network costs of submitting the bet.
|
| In subsequent vote market releases it will be possible to stake $FVT to increase exposure to these bets, however in our early phases users will win shares in the reward pool and gain vote power without the requirement to stake capital.
|
| It is not necessarily the case that those with the most wealth, hold the most knowledge. We use our semantic ballot voting system to aggregate knowledge from a distributed group of crypto users and aim to strip out the plutocratic influence that wealth has on the market, creating a microcosmic merit-based market environment.

A Gamified School of Decentralised Finance
-----------------------------------------------
| The vote markets allow users to engage with the market in a lower risk and downside protected environment, where they can hone their trading skills and build fundamental knowledge about the cryptospace.
|
| finance.vote identity holders will gain access to educational material that aims to boost users’ understanding of the decentralised technologies of the future. It is in the interest of the network to amplify the collective intelligence of its user base and this will take place at first in existing social media platforms and then on our second layer governance system, which will be a consensus curated dialogue and decision framework optimised for this purpose.

Price Discovery
-----------------------------------------------
| It is an existing paradigm in the cryptospace and indeed the wider market system to price  productive enterprise relatively against one another. Is Microsoft better than Google? Is Aave better than Compound? Our vote market system asks this question consistently, cyclically and with a reputation system that ensures that market intelligence is rewarded for these activities.
|
| Market capitalization and other metrics are a poor indicator of quality in the cryptospace, especially as token economics and inflationary dynamics in the token systems often lead to wildly inaccurate initial pricing of tokens.
|
| It is our intention that the vote market system generates alpha based on the fundamental value proposition of tokens in the crypto economy.