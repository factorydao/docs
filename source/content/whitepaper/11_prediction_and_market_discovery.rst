Prediction and Market Discovery
================================
Turning Degens into Alpha
-----------------------------
Problem:

*  The perennial explosion of altcoins produces some winners and mostly losers.
*  The permissionless of Uniswap and other DEXs creates vast, rapidly evolving and noisy markets.
*  Early signals on the potential of token value are dominated by a small number of influencers with ulterior motives [4]_.


| DAOs and prediction market dynamics demonstrate huge potential as tools for improving large scale decision making and governance systems [5]_. However, broad spectrum prediction market systems such as Augur, although maturing, have yet to find meaningful adoption [6]_. Largely, this is due to the open ended parameter space in which predictions can be made, which dilutes liquidity and interest across the markets. finance.vote lenses focus and adoption into a small number of shared vote markets, with constant and far more immediate settlement. Liquidity issues are resolved through the finance.vote token economics, which distributes a small amount of token inflation into reward pools, which seeds liquidity for every vote.
|
| Using the semantic ballot voting system, users are presented with the ability to vote on the future market success (or failure) of tokens from across the cryptospace. This allows users to make market bets across multiple tokens, all contained in a single transaction.
|
| $FVT rewards are claimed from reward pools by users who are correct in their predictions. This causes a progressive aggregation of voting power to those who can consistently make accurate market predictions, chaining prediction market decisions through reputation factors.
|
| As adoption increases, a range of voting markets will be introduced through the finance.vote governance system, which will allow users to accrue reputation in the system based on their ability to accurately predict verifiable on-chain market metrics.


.. [4] “Cryptocurrency Scam As Crypto Influencers Tweet About ....” 15 Jul. 2020, https://www.forbes.com/sites/rogerhuang/2020/07/15/likely-cryptocurrency-scam-as-crypto-exchanges-and-influencers-tweet-about-cryptoforhealth/. *Accessed 26 Aug. 2020.*
.. [5] “DAOs, Democracy and Governance - Ralph Merkle.” 31 May. 2016, https://merkle.com/papers/DAOdemocracyDraft.pdf. *Accessed 26 Aug. 2020.*
.. [6] “Augur Price Analysis- Project matures but user numbers still low.” 7 May. 2019, https://bravenewcoin.com/insights/augur-price-analysis-project-matures-but-user-numbers-still-low. *Accessed 26 Aug. 2020.*