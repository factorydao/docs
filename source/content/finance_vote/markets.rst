Market Links
====================

ETH
-------------
`Sushi swap <https://app.sushi.com/swap?outputCurrency=0x45080a6531d671DDFf20DB42f93792a489685e32&inputCurrency=ETH>`_

`Uniswap <https://app.uniswap.org/#/swap?use=V2&outputCurrency=0x45080a6531d671DDFf20DB42f93792a489685e32&inputCurrency=ETH>`_

BSC
-----------
`PancakeSwap <https://pancakeswap.finance/swap?outputCurrency=0x0A232cb2005Bda62D3DE7Ab5DEb3ffe4c456165a&inputCurrency=BNB>`_

Polygon
--------
`Quickswap <https://quickswap.exchange/#/swap?outputCurrency=0x72a5a58f79FFc2102227B92fAeBA93B169a3A3F1&inputCurrency=ETH>`_