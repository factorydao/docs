Weekly Updates
========================================
2021
------
- :doc:`/weekly/2021_05_may`
- :doc:`/weekly/2021_06_june`
- :doc:`/weekly/2021_07_july`
- :doc:`/weekly/2021_08_august`
- :doc:`/weekly/2021_09_september`
- :doc:`/weekly/2021_10_october`
- :doc:`/weekly/2021_11_november`
- :doc:`/weekly/2021_12_december`

2022
------
- :doc:`/weekly/2022_01_january`
- :doc:`/weekly/2022_02_february`
- :doc:`/weekly/2022_03_march`
- :doc:`/weekly/2022_04_april`
- :doc:`/weekly/2022_05_may`
- :doc:`/weekly/2022_06_june`
- :doc:`/weekly/2022_07_july`
- :doc:`/weekly/2022_08_august`